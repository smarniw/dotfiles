# Setup fzf
# ---------
if [[ ! "$PATH" == */home/swarnim/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/swarnim/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/swarnim/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------
source "/home/swarnim/.fzf/shell/key-bindings.bash"

call plug#begin('/$HOME/.config/nvim/autoload/plugged/')

" Start screen for vim
Plug 'mhinz/vim-startify'

" Modeline
Plug 'itchyny/lightline.vim'

" Make your own theme
Plug 'urbainvaes/vim-macaw'

" File Browsing
Plug 'preservim/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
" Plug 'ctrlpvim/ctrlp.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Editing
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'jiangmiao/auto-pairs'

" Colorschemes
" Tender
Plug 'jacoborus/tender.vim'
" Gruvbox
Plug 'sainnhe/gruvbox-material'
Plug 'morhetz/gruvbox'
" Sonokai
Plug 'sainnhe/sonokai'
" Forest Night
Plug 'sainnhe/forest-night'
" Edge
Plug 'sainnhe/edge'

" CSS colors
Plug 'ap/vim-css-color'

"Syntax Highlight Pack
Plug 'sheerun/vim-polyglot'

" Checking register contents of " or @
Plug 'junegunn/vim-peekaboo'
" Paranthesis
Plug 'junegunn/rainbow_parentheses.vim'
" Indent guides
Plug 'juniway/indent-bar'
" Digraph
Plug 'DrCracket/painless-digraph'
call plug#end()


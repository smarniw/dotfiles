"__   __ (_)  _ __ ___  
"\ \ / / | | | '_ ` _ \ 
" \ V /  | | | | | | | |
"  \_/   |_| |_| |_| |_|
"                         __   _         
"  ___    ___    _ __    / _| (_)   __ _ 
" / __|  / _ \  | '_ \  | |_  | |  / _` |
"| (__  | (_) | | | | | |  _| | | | (_| |
" \___|  \___/  |_| |_| |_|   |_|  \__, |
"                                  |___/ 
"
"""""""""""""""""""""""""""""""""""""""""""""""""""""

" Vim Plug
source ~/.config/nvim/vim-plug/plugins.vim

" General settings
source ~/.config/nvim/general/settings.vim
source ~/.config/nvim/general/aucommands.vim

" Themes
" source ~/.config/nvim/themes/gruvbox.vim
source ~/.config/nvim/themes/sonokai.vim
" source ~/.config/nvim/themes/forest-night.vim
" source ~/.config/nvim/themes/edge.vim

" Plug Specific Configs

" Rainbow Paranthesis
source ~/.config/nvim/plug-specific-configs/rainbow-parantheses.vim

" NerdTree
source ~/.config/nvim/plug-specific-configs/nerdtree.vim

" LightLine
source ~/.config/nvim/plug-specific-configs/lightline.vim

" Ctrl-P
source ~/.config/nvim/plug-specific-configs/ctlp.vim

" Fzf Vim
source ~/.config/nvim/plug-specific-configs/fzf.vim

" Indent Guides
source ~/.config/nvim/plug-specific-configs/indent-guides.vim

" Painless diagraph
source ~/.config/nvim/plug-specific-configs/painlessdiagraph.vim

"""""""""""""""""""""""""""""
"" Key bindings
"""""""""""""""""""""""""""""

" Split navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-j> <C-w>k
map <C-l> <C-w>l

" Open vertical split
noremap <leader>v <C-w>v
noremap <leader>s <C-w>s

" go to beginning and end of line quickly
nnoremap H ^
nnoremap L g_
vnoremap L g_

" keep search term in mid part of screen
nnoremap n nzzzv
nnoremap N Nzzzv


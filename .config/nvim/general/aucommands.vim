" disable autocommenting next line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o


" Resize splits when the window is resized
au VimResized * :wincmd =


" Make sure Vim returns to the same line when you reopen a file.
augroup line_return
    au!
    au BufReadPost *
        \ if line("'\"") > 0 && line("'\"") <= line("$") |
        \     execute 'normal! g`"zvzz' |
        \ endif
augroup END

set termguicolors
let mapleader = " "
set showtabline=1
set nowrap
set encoding=utf-8
set mouse=a
set autoindent
set nohlsearch
set ruler
set cursorline
syntax enable
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set number relativenumber
set smartcase
set noswapfile
set nobackup
set incsearch
filetype plugin indent on
set wildmenu
set wildmode=longest:list,full
set history=35
set showcmd
set nolangremap
map Q gq
set cmdheight=1
set splitbelow splitright
set noshowmode
set laststatus=2
set scrolloff=8
set updatetime=50
set shm=at
set formatoptions+=j " Delete comment character when joining commented lines
set autoread
set autowrite
set colorcolumn=80
highlight ColorColumn ctermbg=0 guibg=lightgrey
set list                              " show whitespace
set listchars=nbsp:⦸,tab:>-,extends:»,precedes:«,trail:•

" Shortens messages to avoid 'press a key' prompt
set shortmess=aoOtTI

if has('linebreak')
  let &showbreak='⤷ '
endif


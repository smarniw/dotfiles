map <C-n> :NERDTreeToggle<CR>

" open nerdtree when no files were specified
" autocmd StdinReadPre * let s:std_in=1
" autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

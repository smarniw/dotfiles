" let g:sonokai_style = 'maia'
let g:sonokai_style = 'atlantis'
" let g:sonokai_style = 'andromeda'
" let g:sonokai_style = 'shusia'
let g:sonokai_transparent_background = 0
let g:sonokai_enable_italic = 1
let g:sonokai_disable_italic_comment = 1
colorscheme sonokai

# Added by me
autoload -Uz promptinit
promptinit
prompt suse
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/aliasrc"

ttyctl -f
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[red]%} %{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M$fg[red]%}==>%{$reset_color%}%b "
# PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "
# PS1='%B%(?,,[%?] )%3~%# %b'

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line




# The following lines were added by compinstall

zstyle ':completion:*' completer _list _expand _complete _ignored
zstyle ':completion:*' condition 0
zstyle ':completion:*' expand prefix
zstyle ':completion:*' file-sort access
zstyle ':completion:*' format '`completing %d'\'''
zstyle ':completion:*' group-name ''
zstyle ':completion:*' ignore-parents parent pwd directory
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' list-suffixes true
zstyle ':completion:*' matcher-list '' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-/]=* r:|=*'
zstyle ':completion:*' menu select=0
zstyle ':completion:*' preserve-prefix '//[^/]##/'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/swarnim/.zshrc'

autoload -Uz compinit
compinit
_comp_options+=(globdots)		# Include hidden files.
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTSIZE=10000
SAVEHIST=$HISTSIZE
HISTFILE=$HOME/.zsh_history
setopt autocd nomatch
setopt extended_glob
unsetopt beep notify
bindkey -v
# End of lines configured by zsh-newuser-install

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh


# FZF
export FZF_DEFAULT_COMMAND='rg --hidden --no-ignore --files'
export FZF_DEFAULT_OPTS='--height 40%
    --layout=reverse
    --multi
    --border
    --color fg:#ebdbb2,bg:#1d2021,hl:#fabd2f,fg+:#ebdbb2,bg+:#3c3836,hl+:#fabd2f
    --color info:#83a598,prompt:#bdae93,spinner:#fabd2f,pointer:#83a598,marker:#fe8019,header:#665c54'

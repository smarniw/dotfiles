#!/bin/bash
#
# ~/.bashrc
#
# export TERM=kitty
# export TERM=rxvt
export EDITOR=nvim
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8
# export TERM=xterm-256color
export CLICOLOR=1
export PATH
export MANPATH=/usr/share/man

blox
# sysinfo
# paleofetch
# archey
# rsfetch -edPklrtuw -L .config/neofetch/arch3
# source ~/.bash-powerline.sh
source ~/.config/aliasrc
source ~/.config/dotbare/dotbare.plugin.bash
if [ -d "$HOME/.local/bin/" ] ; then
  PATH="$PATH:$HOME/.local/bin/"
fi

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '


## Program settings
# FZF
source .config/fzf/config

[ -f ~/.fzf.bash ] && source ~/.config/fzf/.fzf.bash

shopt -s autocd

# Kitty 
source <(kitty + complete setup bash)


# exporting paths
if [ -d "$HOME/.local/bin/" ] ; then
  PATH="$PATH:$HOME/.local/bin/"
fi

export PATH="~/.local/bin/.scripts/fzf/:$PATH"
export PATH="~/.local/bin/.scripts/:$PATH"

# bind
bind "set completion-ignore-case on"
# bind 'TAB:menu-complete'
bind "set completion-map-case on"
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=/home/swarnim'
